/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var url = "resource/data.json";
var amount = 0;
var img_url = "";
var imgg = "";


xmlhttp.onreadystatechange = function () {

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        var myArray = JSON.parse(xmlhttp.responseText);
        //displayMyThings(myArray);
        init();
        displayMyThingsDB();
    }
};

function addItem() {
    var id = amount;
    var obj = document.getElementById('gegenstand').value;
    var img = imgg;
    var besch = document.getElementById('beschreibung').value;
    var vor = document.getElementById('vorname').value;
    var name = document.getElementById('name').value;
    var tel = document.getElementById('telefon').value;
    
    addItemIntoDB(id,obj,img,besch,vor,name,tel);
    
    amount++;
    //syncDB();
    displayMyThingsDB();
}

xmlhttp.open("GET", url, true);
xmlhttp.send();
$('#page').on('pageinit', function () {
    $("#chooseFile").click(function (e) {
        e.preventDefault();
        $("input[type=file]").trigger("click");
    });
    $("input[type=file]").change(function () {
        var file = $("input[type=file]")[0].files[0];
        $("#preview").empty();
        displayAsImage3(file, "preview");
    });
});

function displayAsImage3(file, containerid) {
    if (typeof FileReader !== "undefined") {
        var container = document.getElementById(containerid),
                img = document.createElement("img"),
                reader;
        container.appendChild(img);
        reader = new FileReader();
        reader.onload = (function (theImg) {
            return function (evt) {
                theImg.src = evt.target.result;
                //alert("imgsrc"+theImg.src);
                imgg = theImg.src;
            };
        }(img));
        reader.readAsDataURL(file);
    }
}

$('#new').click(function () {
    window.location.href = '#page';
    return false;
});
// Map initialitation ---------------------------------------------------------
var x = document.getElementById("demo");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    var latlon = position.coords.latitude + "," + position.coords.longitude;
    img_url = "http://maps.googleapis.com/maps/api/staticmap?center="
            + latlon + "&zoom=14&size=260x160&sensor=false";
    document.getElementById("mapholder").innerHTML = "<img src='" + img_url + "'>";
    console.log(img_url);
}

function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable.";
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred.";
            break;
    }
}
// Map initialitation ---------------------------------------------------------

function displayMyThingsDB() {
    var out = '';
    var search = document.getElementById('search').value;
    document.getElementById("artikel").innerHTML = '';
    loadArticle('1', out,search);
}

function getObjInfos(ObjName) {
   getObjInfosDB(ObjName);
}

function test(){
    
 alert("hallo from TestFile");
 
 addItemIntoDB('3','Testfile','img','Test','hans','muster','55555');
 console.log('added new test');
 displayMyThingsDB();
 console.log('display');
 alert("Test done");
 amount++;
}